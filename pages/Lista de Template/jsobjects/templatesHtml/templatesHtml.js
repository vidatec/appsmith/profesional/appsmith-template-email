export default {
  generic() {
    return `
<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml" lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="x-apple-disable-message-reformatting">
  <!--[if mso]> 
<noscript> 
<xml> 
<o:OfficeDocumentSettings> 
<o:PixelsPerInch>96</o:PixelsPerInch> 
</o:OfficeDocumentSettings> 
</xml> 
</noscript> 
<![endif]-->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500&display=swap" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet" />
</head>
<body style="max-width: 700px;margin: 0 auto">
<main style="font-family: 'Roboto', sans-serif;margin: 0 auto;text-align: center;width: 100%;padding: 0px;box-sizing: border-box;background-color:#F9FAFB;">
  <header style="background-color: transparent;width: 100%;height: 89px;display: table;border-bottom: 2px solid #c4c4c4;">
    <div style=" display: table-cell;vertical-align: middle;text-align: center;">
      <img style="object-fit: cover;height: 59px;margin: 0 auto;display: block;" src={{Form1.data.image}}  alt="imagen-logo"/>
</div>
</header>
  <div style="color: #000000;padding-top: 16px;padding-bottom: 16px;text-align: center;font-style: normal;">
    <h2 style="font-size: 20px;font-weight: 700;line-height: 30px;text-transform: capitalize;margin: 0px;">
      ¡Hola, #user_name!</h2>
    <h3 style="margin: 0;font-size: 14px;letter-spacing: 0.1px;line-height: 22px;font-weight: 400;white-space: pre-wrap;padding: 10px;font-weight: {{SwitchTitle.isSwitchedOn ? '700' : '400'}}">{{Form1.data.title}}</h3>
    <a style="background-color: {{Form1.data.color ? Form1.data.color : '#000066'}};color: #ffffff;font-size: 14px;font-weight: 500;border-radius: 20px;text-decoration: none;height: 37px;line-height: 37px;display: inline-block;padding: 0 24px;text-transform: uppercase;cursor: pointer;box-sizing: border-box;" target="_blank" href="#button_action">Confirmar registro</a>
<div style="font-size: 14px;letter-spacing: 0.1px;line-height: 22px;font-weight: 400;white-space: pre-wrap;padding: 10px;min-height: 150px;"><div style="padding: 0 10px;">
{{ Form1.data.description1 ? (SwitchDescription1.isSwitchedOn ?
															<strong>${Form1.data.description1}</strong> : <p style="margin: 0;">${
      Form1.data.description1
    }</p>)  
: ''
}}{{ Form1.data.link ? 
<a style="color: ${
      Form1.data.color ? Form1.data.color : "#000066"
    };cursor:pointer;" href="${Form1.data.link}" target="_blank">${
      Form1.data.link
    }</a> 
: '' }}

{{ Form1.data.description2 ? (SwitchDescription2.isSwitchedOn ?
															<strong>${Form1.data.description2}</strong> : <p style="margin: 0;">${
      Form1.data.description2
    }</p>)  
: ''
}}
{{ Form1.data.description3 ? (SwitchDescription3.isSwitchedOn ?
															<strong>${Form1.data.description3}</strong> : <p style="margin: 0;">${
      Form1.data.description3
    }</p>)  
: ''
}}
</div>
</div>
</div></div>
<footer style="text-align: center;min-height: 75px;background: #2d2d2d;font-family: 'Inter', sans-serif;color: #ffffff;white-space: pre-line;padding: 16px">
<p style="margin: 0 auto;line-height: 21px;font-size: 11px;">{{Form1.data.footer}} <a style="color: #ffffff;cursor: pointer;" href="mailto:soporteacademy@ticmas.com" target="_blank">
soporteacademy@ticmas.com</a></p></footer>
</main>
</body>
</html>`;
  },
};
